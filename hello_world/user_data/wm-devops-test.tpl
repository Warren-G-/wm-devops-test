#cloud-config
runcmd:
  - sudo apt update
  - sudo apt install nginx -y
  - sudo chown -R admin:admin /var/www/html
  - sudo chmod -R 755 /var/www/html
  - sudo echo "Hello World" > index.html
  - sudo mv index.html /var/www/html/
  - sudo systemctl start nginx
