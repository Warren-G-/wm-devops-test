terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "wm-devops-terraform-state"
    dynamodb_table = "wm-devops-terraform-state"
    region         = "ap-southeast-2"
    key            = "vpc-full/tfstate"
    profile        = "default"
  }
}

# data "terraform_remote_state" "remote_state" {
#   backend = "s3"
#   config = {
#     bucket  = "wm-devops-terraform-state"
#     key     = "remote-state/tfstate"
#     region  = "ap-southeast-2"
#     profile = "default" 
#   }
# }

provider "aws" {
  region  = "ap-southeast-2"
  profile = "default"
}

