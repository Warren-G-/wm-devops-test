data "aws_availability_zones" "available" {}

############################################
## VPC Customisations for Warren DevOps Test
############################################

module "wm-devops-test-vpc" {
source = "git::git@bitbucket.org:Warren-G-/vpc-module.git"

aws_region = "ap-southeast-2"
aws_zones = data.aws_availability_zones.available.names
vpc_cidr = "10.0.0.0/16"
vpc_name = "wm-devops-test-vpc"
private_subnets = true
tags = {
    Environment = "wm-devops-test" 
    Cost = "wm-devops-test"
  } 
}

##############################################
## Attach Rules Specific to Warren DevOps Test
##############################################
 resource "aws_network_acl_rule" "public_acl_https_out" {
  network_acl_id = module.wm-devops-test-vpc.public_acl
  rule_number    = 100
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 443
  to_port        = 443
}

resource "aws_network_acl_rule" "public_acl_http_out" {
  network_acl_id = module.wm-devops-test-vpc.public_acl
  rule_number    = 200
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}

resource "aws_network_acl_rule" "public_acl_return_out" {
  network_acl_id = module.wm-devops-test-vpc.public_acl
  rule_number    = 300
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}


resource "aws_network_acl_rule" "public_acl_http_in" {
  network_acl_id = module.wm-devops-test-vpc.public_acl
  rule_number    = 100
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}

resource "aws_network_acl_rule" "public_acl_https_in" {
  network_acl_id = module.wm-devops-test-vpc.public_acl
  rule_number    = 200
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 443
  to_port        = 443
}

resource "aws_network_acl_rule" "public_acl_return_in" {
  network_acl_id = module.wm-devops-test-vpc.public_acl
  rule_number    = 300
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}

 resource "aws_network_acl_rule" "private_acl_https_out" {
  network_acl_id = module.wm-devops-test-vpc.private_acl
  rule_number    = 100
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 443
  to_port        = 443
}

resource "aws_network_acl_rule" "private_acl_http_out" {
  network_acl_id = module.wm-devops-test-vpc.private_acl
  rule_number    = 200
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}

resource "aws_network_acl_rule" "private_acl_return_out" {
  network_acl_id = module.wm-devops-test-vpc.private_acl
  rule_number    = 300
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}

resource "aws_network_acl_rule" "private_acl_http_in" {
  network_acl_id = module.wm-devops-test-vpc.private_acl
  rule_number    = 100
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.0.0/16"
  from_port      = 80
  to_port        = 80
}

resource "aws_network_acl_rule" "private_acl_https_in" {
  network_acl_id = module.wm-devops-test-vpc.private_acl
  rule_number    = 200
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.0.0/16"
  from_port      = 443
  to_port        = 443
}

resource "aws_network_acl_rule" "private_acl_return_in" {
  network_acl_id = module.wm-devops-test-vpc.private_acl
  rule_number    = 300
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}


#####


resource "aws_security_group" "wm_devops_test_private_sg" {
  name        = "wm_devops_test__sg"
  description = "Allow TLS inbound traffic"
  vpc_id      =  module.wm-devops-test-vpc.vpc_id

  ingress {
    description = "HTTP inbound from ALB"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

    egress {
    description = "HTTPS outbound"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   egress {
    description = "HTTP Outbound"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   
  }
  tags = {
        Name = "wm_devops_test_alb_sg"
    }

  }

  resource "aws_security_group" "wm_devops_test_alb_sg" {
  name        = "wm_devops_test_alb_sg"
  description = "Allow TLS inbound traffic"
  vpc_id      =  module.wm-devops-test-vpc.vpc_id

  ingress {
    description = "HTTP from anywhere to ALB"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "TLS from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
        Name = "wm_devops_test_alb_sg"
    }

  }



resource "aws_launch_template" "wm_devops_test_lt" {
  name_prefix   = "wm-devops-test"
  image_id      = "ami-07fc3dfb32d660df7"
  instance_type = "t2.nano"
  vpc_security_group_ids = [aws_security_group.wm_devops_test_private_sg.id]
  user_data =   data.template_file.deploy.rendered
  monitoring {
    enabled = true
  }
 
} 
data "template_file" "deploy" {
  template = filebase64("./user_data/wm-devops-test.tpl")

}
  

   



resource "aws_lb_target_group" "wm_devops_test_tg" {
  name     = "wm-devops-test-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = module.wm-devops-test-vpc.vpc_id
  
  health_check {
    interval             = "20"
    path                 = "/"
    port                 = "80"
    protocol             = "HTTP"
    timeout              = "5"
    healthy_threshold    = "2"
    unhealthy_threshold  = "3"
    matcher              = "200"

  }
}

resource "aws_lb" "wm_devops_test_alb" {
  name               = "wm-devops-test-alb"
  internal           = "false"
  load_balancer_type = "application"
  security_groups    = aws_security_group.wm_devops_test_alb_sg.id[*]
  subnets            = module.wm-devops-test-vpc.public_subnet_ids


}

resource "aws_lb_listener" "wm_devops_test_alb_http" {
  load_balancer_arn = aws_lb.wm_devops_test_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.wm_devops_test_tg.arn
    type             = "forward"
  }
}

resource "aws_autoscaling_group" "wm_devops_test_asg" {
  name                      = "wm-devops-test-asg"
  max_size                  = 6
  min_size                  = 3
  health_check_grace_period = 300
  health_check_type         = "ELB"
  target_group_arns         = [aws_lb_target_group.wm_devops_test_tg.arn]
  desired_capacity          = 3
  force_delete              = true
  #placement_group          = "${aws_placement_group.test.id}"
  vpc_zone_identifier       = module.wm-devops-test-vpc.private_subnet_ids[*]
  
launch_template {
    id      = aws_launch_template.wm_devops_test_lt.id
    version = "$Latest"
  }
}

resource "aws_autoscaling_policy" "wm_devops_test_asg_policy" {
  name                      = "wm_devops_test_asg_policy"
  autoscaling_group_name    = aws_autoscaling_group.wm_devops_test_asg.name
  estimated_instance_warmup = 120
  policy_type               = "TargetTrackingScaling"
  

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = "90"
  }
}