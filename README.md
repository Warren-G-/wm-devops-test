# About #

This project and associated repos have been created as part of a devops test for Warren MacDougall.

### Part 1 ###

# Deliverables #

- "Hello World" web page that scales to meet demand
- Environment that adhers to AWS best practices 


# Design #

There are a number of resources involved in delivering the solution but overall its relatively straight forward.


![picture](wm-devops-test.png)



### How To Deploy ###

Prerequisites:

AWS CLI 
Terraform =>v0.12
Git
AWS programatic access with administrative access. 

Steps:

 * Clone this git repo -> git clone git@bitbucket.org:Warren-G-/wm-devops-test.git  
  
 * From the repo directory move to the S3 folder and run "terraform init" this will initialise the backend and download any required modules.  
  
 * In the S3 folder run "terraform apply" review the resources to be deployed, if satisfactory answer "yes" when prompted to continue.  
  
 * When step 3 is complete move to the folder wm-devops-test and run "terraform init" again to initizlise and download the required module.  
   
 * Run "terraform apply" and confirm you're happy to deploy the listed resources when prompted.  
   
 * When the evaluation is complete and the resources are no longer required run "terraform destroy" and answer "yes" if you're sure the  
   resources are no longer required. 

 * Open your AWS console and browse to the "EC2" section where you will find "load balancers". Click on the "wm-devops-test" load balancer  
   and copy the DNS name. Paste the DNS name into your browser to see the "Hello World". Please note it will take several minutes for the   
   content to be served up (you may see some unhealthy instances in the target group intially, this will sort itself).  
   
 * Questions or issues please call Warren @ 0402645044 :)

### Part 2  ###

* How would you provide shell access into the application stack for operations staff who may want to log into an instance.  
  
  See Diagram (mgmt subnet) example.

Answer A

* Ideally shell access would be provided over a VPN between an office network and AWS. This is ideal as office users have direct access to AWS  
  and if there is an existing office VPN they will have access via that VPN as well.  
   
*  Second option would be to use a split tunnel VPN (OpenVPN or some other VPN solution) to a management subnet. From there administrators can   
   access each of the subnets.
      
*  Lastly is the trusty old bastion host. There are a number of hardened AMI's on the AWS marketplace for use as bastion hosts.


* Make access and error logs available in CloudWatch Logs

Answer B

* To make access and error logs available in Cloudwatch logs we would need a role with access to cloudwatch logs. The Cloudwatch logs agent   
  then needs to be installed and configured to forward logs to cloudwatch. For better understanding of access we could also enable VPC flow logs,  
  this would require an additional role for the VPC.


### Notes  ###

- The solution is missing a few items that should be highlighted
  - No Route53 records were created as we do not have a domain we can create a cname record for to point to the load balancer
  - For the same reason as above we don't have TLS enabled, we could have put a cert on the ALB but would have received a mismatch althought   
    technically still secure.
  - Encryption was not applied to the EC2 EBS volumes as the instances were created from unencrypted snapshots. unfortunately there was   
    insufficient time to customize one.