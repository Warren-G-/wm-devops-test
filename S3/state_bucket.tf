provider "aws" {
  region  = "ap-southeast-2"
  profile = "default"
}


resource "aws_s3_bucket" "tf_state_bucket" {
  bucket = "wm-devops-terraform-state"
  acl    = "private"

  versioning {
    enabled = true
  }
server_side_encryption_configuration {
      rule {
      apply_server_side_encryption_by_default {
           sse_algorithm     = "aws:kms"
    }
    }   
 }
}


